package com.btmatthews.maven.plugins.cms.opencmis;

import com.btmatthews.utils.monitor.Logger;
import com.btmatthews.utils.monitor.Server;

/**
 * Created by bmatthews68 on 12/02/2014.
 */
public class OpenCMISServer implements Server {

    private org.eclipse.jetty.server.Server jettyServer;

    @Override
    public void configure(final String name, Object value, Logger logger) {

    }

    @Override
    public void start(final Logger logger) {
        jettyServer = new org.eclipse.jetty.server.Server();
        try {
            jettyServer.start();
        } catch (final Exception e) {
            logger.logError("Error launching down embedded web server", e);
        }
    }

    @Override
    public boolean isStarted(final Logger logger) {
        return jettyServer != null && jettyServer.isStarted();
    }

    @Override
    public void stop(final Logger logger) {
        if (jettyServer != null) {
            try {
                jettyServer.stop();
            } catch (final Exception e) {
                logger.logError("Error shutting down embedded web server", e);
            }
        }
    }

    @Override
    public boolean isStopped(final Logger logger) {
        return jettyServer == null || jettyServer.isStopped();
    }
}
